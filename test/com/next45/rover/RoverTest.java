package com.next45.rover;

import com.next45.commands.ControlRoom;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

/***
 * This is a test class for the Rover Object
 */
public class RoverTest {

    //Constants to Test Rover Movement
    private static final String[] moveOneStepNorth={"88", "11 N", "M"};
    private static final String[] moveOneStepSouth={"88", "11 S", "M"};
    private static final String[] moveOneStepEast={"88", "11 E", "M"};
    private static final String[] moveOneStepWest={"88", "11 W", "M"};

    //Constants to Test Rover Rotation
    private static final String[] rotateNorthToEast={"88", "11 N", "R"};
    private static final String[] rotateNorthToWest={"88", "11 N", "L"};

    private static final String[] rotateEastToNorth={"88", "11 E", "L"};
    private static final String[] rotateEastToSouth={"88", "11 E", "R"};

    private static final String[] rotateSouthToEast={"88", "11 S", "L"};
    private static final String[] rotateSouthToWest={"88", "11 S", "R"};

    private static final String[] rotateWestToSouth={"88", "11 W", "L"};
    private static final String[] rotateWestToNorth={"88", "11 W", "R"};

    //Constants for Acceptance Testing
    private static final String[] acceptanceTestCase1={"88", "12 E", "MMLMRMMRRMML"};
    private static final String[] acceptanceTestCase2={"55", "12 N", "LMLMLMLMM"};
    private static final String[] acceptanceTestCase3={"55", "33 E", "MMRMMRMRRM"};
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("Testing the Rover Class");
        System.out.println("----------------------");
        System.out.println();

        System.out.println("Setting up Before Class");
        System.out.println();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("Tearing down after test");
        System.out.println("Tearing down complete");
    }

    //Test Movement

    /***
     * This test checks whether the Rover can move one step to the North
     */
    @Test
    public void roverMoveOneStepNorth() {
        Rover testRover = new Rover(new ControlRoom(moveOneStepNorth));
        assertReflectionEquals(testRover.move(), "12 N");
    }

    /***
     * This test checks whether the Rover can move one step to the South
     */
    @Test
    public void roverMoveOneStepSouth() {
        Rover testRover = new Rover(new ControlRoom(moveOneStepSouth));
        assertReflectionEquals(testRover.move(), "10 S");
    }

    /***
     * This test checks whether the Rover can move one step to the East
     */
    @Test
    public void roverMoveOneStepEast() {
        Rover testRover = new Rover(new ControlRoom(moveOneStepEast));
        assertReflectionEquals(testRover.move(), "21 E");
    }

    /***
     * This test checks whether the Rover can move one step to the West
     */
    @Test
    public void roverMoveOneStepWest() {
        Rover testRover = new Rover(new ControlRoom(moveOneStepWest));
        assertReflectionEquals(testRover.move(), "01 W");
    }

    //Test Rotation
    /***
     * This test checks whether the Rover can Rotate from North and face East
     */
    @Test
    public void roverRotateNorthToEast() {
        Rover testRover = new Rover(new ControlRoom(rotateNorthToEast));
        assertReflectionEquals(testRover.move(), "11 E");
    }

    /***
     * This test checks whether the Rover can Rotate from North and face West
     */
    @Test
    public void roverRotateNorthToWest() {
        Rover testRover = new Rover(new ControlRoom(rotateNorthToWest));
        assertReflectionEquals(testRover.move(), "11 W");
    }

    /***
     * This test checks whether the Rover can Rotate from East and face North
     */
    @Test
    public void roverRotateEastToNorth() {
        Rover testRover = new Rover(new ControlRoom(rotateEastToNorth));
        assertReflectionEquals(testRover.move(), "11 N");
    }

    /***
     * This test checks whether the Rover can Rotate from East and face South
     */
    @Test
    public void roverRotateEastToSouth() {
        Rover testRover = new Rover(new ControlRoom(rotateEastToSouth));
        assertReflectionEquals(testRover.move(), "11 S");
    }

    /***
     * This test checks whether the Rover can Rotate from South and face East
     */
    @Test
    public void roverRotateSouthToEast() {
        Rover testRover = new Rover(new ControlRoom(rotateSouthToEast));
        assertReflectionEquals(testRover.move(), "11 E");
    }

    /***
     * This test checks whether the Rover can Rotate from South and face West
     */
    @Test
    public void roverRotateSouthToWest() {
        Rover testRover = new Rover(new ControlRoom(rotateSouthToWest));
        assertReflectionEquals(testRover.move(), "11 W");
    }

    /***
     * This test checks whether the Rover can Rotate from West and face South
     */
    @Test
    public void roverRotateWestToSouth() {
        Rover testRover = new Rover(new ControlRoom(rotateWestToSouth));
        assertReflectionEquals(testRover.move(), "11 S");
    }

    /***
     * This test checks whether the Rover can Rotate from West and face North
     */
    @Test
    public void roverRotateWestToNorth() {
        Rover testRover = new Rover(new ControlRoom(rotateWestToNorth));
        assertReflectionEquals(testRover.move(), "11 N");;
    }

    /***
     * This test checks whether the Rover moves to the desired position
     */
    @Test
    public void acceptanceTestCase1() {
        Rover testRover = new Rover(new ControlRoom(acceptanceTestCase1));
        assertReflectionEquals(testRover.move(), "33 S");;
    }

    /***
     * This test checks whether the Rover moves to the desired position
     */
    @Test
    public void acceptanceTestCase2() {
        Rover testRover = new Rover(new ControlRoom(acceptanceTestCase2));
        assertReflectionEquals(testRover.move(), "13 N");;
    }

    /***
     * This test checks whether the Rover moves to the desired position
     */
    @Test
    public void acceptanceTestCase3() {
        Rover testRover = new Rover(new ControlRoom(acceptanceTestCase3));
        assertReflectionEquals(testRover.move(), "51 E");;
    }

}