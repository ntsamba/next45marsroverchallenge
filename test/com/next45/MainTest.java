package com.next45;

import com.next45.commands.ControlRoom;
import org.junit.*;

import static org.junit.Assert.*;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

public class MainTest {

    //Constants to Test the Zone
    private static final String[] positiveZone={"88", "12 E", "MLLRRMMMMR"};
    private static final String[] positiveZone1={"8982", "12 E", "MLLRRMMMMR"};
    private static final String[] positiveZone2={"4850", "12 E", "MLLRRMMMMR"};
    private static final String[] positiveZone3={"00", "12 E", "MLLRRMMMMR"};

    private static final ControlRoom positiveZoneCR = new ControlRoom(positiveZone);
    private static final ControlRoom positiveZoneCR1= new ControlRoom(positiveZone1);
    private static final ControlRoom positiveZoneCR2= new ControlRoom(positiveZone2);
    private static final ControlRoom positiveZoneCR3= new ControlRoom(positiveZone3);

    private static final String[] incorrectZone={"889", "12 E", "MLLRRMMMMR"};
    private static final String[] incorrectZone1={"-29", "12 E", "MLLRRMMMMR"};
    private static final String[] incorrectZone2={"-249", "12 E", "MLLRRMMMMR"};
    private static final String[] incorrectZone3={"8A", "12 E", "MLLRRMMMMR"};

    //Constants to Test the Starting Position
    private static final String[] positiveStartingPosition={"88", "12 E", "MMLMRMMRRMML"};
    private static final String[] positiveStartingPosition1={"88", "88 N", "MMLMRMMRRMML"};
    private static final String[] positiveStartingPosition2={"88", "22 S", "MMLMRMMRRMML"};
    private static final String[] positiveStartingPosition3={"88", "79 W", "MMLMRMMRRMML"};

    private static final ControlRoom positiveStartingPositionCR = new ControlRoom(positiveStartingPosition);
    private static final ControlRoom positiveStartingPositionCR1 = new ControlRoom(positiveStartingPosition1);
    private static final ControlRoom positiveStartingPositionCR2 = new ControlRoom(positiveStartingPosition2);
    private static final ControlRoom positiveStartingPositionCR3 = new ControlRoom(positiveStartingPosition3);

    private static final String[] incorrectStartingPosition={"88", "12 D", "MMLMRMMRRMML"};
    private static final String[] incorrectStartingPosition1={"88", "12 N W ", "MMLMRMMRRMML"};
    private static final String[] incorrectStartingPosition2={"88", "-12 E", "MMLMRMMRRMML"};
    private static final String[] incorrectStartingPosition3={"88", "1A E", "MMLMRMMRRMML"};
    private static final String[] incorrectStartingPosition4={"88", "A2 E", "MMLMRMMRRMML"};
    private static final String[] positiveStartingPosition5={"88", "  79 W   ", "MMLMRMMRRMML"};

    //Constants to Test the Movement
    private static final String[] positiveMovement={"88", "12 E", "mmlmrmmrrmml".toUpperCase()};
    private static final String[] positiveMovement1={"88", "12 E", "MMMMMMM"};
    private static final String[] positiveMovement2={"88", "12 E", "RRRRRRRRRRR"};
    private static final String[] positiveMovement3={"88", "12 E", "LLLLLLLLLLLL"};

    private static final ControlRoom positiveMovementCR = new ControlRoom(positiveMovement);
    private static final ControlRoom positiveMovementCR1 = new ControlRoom(positiveMovement1);
    private static final ControlRoom positiveMovementCR2 = new ControlRoom(positiveMovement2);
    private static final ControlRoom positiveMovementCR3 = new ControlRoom(positiveMovement3);

    private static final String[] incorrectMovement={"88", "12 E", "MMLMRDMMRRMML"};
    private static final String[] incorrectMovement1={"88", "12 E", "mmllrr9m"};
    private static final String[] incorrectMovement2={"88", "12 E", "mmlmrmmrrmml"};
    private static final String[] incorrectMovement3={"88", "12 E", ""};

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("Testing the Main Class");
        System.out.println("----------------------");
        System.out.println();

        System.out.println("Setting up Before Class");
        System.out.println();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("Tearing down after test");
        System.out.println("Tearing down complete");
    }

    @Test
    public void main() {
    }

    private void testHeader(String header){
        System.out.println("Test: "+ header);
        String underline = "";
        for (int index = 0; index < header.length()+6; index++)
            underline = underline + '-';
        System.out.println(underline);
    }

    private void testFooter(String footer){
        System.out.printf("Testing Complete for %s Test", footer);
        System.out.println();
        System.out.println();
    }
    @Test
    public void testPositiveValidateCommandMethod(){

    }

    @Test
    public void testNegativeValidateCommandMethod(){

    }

    @Test
    public void testPositiveValidateZoneMethod(){
        String testName = "Correct Values for the Zone";
        testHeader(testName);
        Assert.assertNotNull(Main.validateCommand(positiveZone));
        Assert.assertNotNull(Main.validateCommand(positiveZone1));
        Assert.assertNotNull(Main.validateCommand(positiveZone2));
        Assert.assertNotNull(Main.validateCommand(positiveZone3));

        assertReflectionEquals(positiveZoneCR, Main.validateCommand(positiveZone));
        assertReflectionEquals(positiveZoneCR1, Main.validateCommand(positiveZone1));
        assertReflectionEquals(positiveZoneCR2, Main.validateCommand(positiveZone2));
        assertReflectionEquals(positiveZoneCR3, Main.validateCommand(positiveZone3));

        testFooter(testName);
    }


    @Test
    public void testNegativeValidateZoneMethod(){
        String testName = "Incorrect Values for the Zone";
        testHeader(testName);
        Assert.assertEquals(null, Main.validateCommand(incorrectZone));
        System.out.println();
        Assert.assertEquals(null, Main.validateCommand(incorrectZone1));
        System.out.println();
        Assert.assertEquals(null, Main.validateCommand(incorrectZone2));
        System.out.println();
        Assert.assertEquals(null, Main.validateCommand(incorrectZone3));
        System.out.println();
        testFooter(testName);
    }

    @Test
    public void testPositiveStartingPositionMethod(){
        String testName = "Correct Starting Positions";
        testHeader(testName);
        Assert.assertNotNull(Main.validateCommand(positiveStartingPosition));
        Assert.assertNotNull(Main.validateCommand(positiveStartingPosition1));
        Assert.assertNotNull(Main.validateCommand(positiveStartingPosition2));
        Assert.assertNotNull(Main.validateCommand(positiveStartingPosition3));

        assertReflectionEquals(positiveStartingPositionCR, Main.validateCommand(positiveStartingPosition));
        assertReflectionEquals(positiveStartingPositionCR1, Main.validateCommand(positiveStartingPosition1));
        assertReflectionEquals(positiveStartingPositionCR2, Main.validateCommand(positiveStartingPosition2));
        assertReflectionEquals(positiveStartingPositionCR3, Main.validateCommand(positiveStartingPosition3));
        testFooter(testName);
    }

    @Test
    public void testNegativeStartingPositionMethod(){
        String testName = "Incorrect Starting Positions";
        testHeader(testName);
        Assert.assertEquals(null, Main.validateCommand(incorrectStartingPosition));
        System.out.println();
        Assert.assertEquals(null, Main.validateCommand(incorrectStartingPosition1));
        System.out.println();
        Assert.assertEquals(null, Main.validateCommand(incorrectStartingPosition2));
        System.out.println();
        Assert.assertEquals(null, Main.validateCommand(incorrectStartingPosition3));
        System.out.println();
        Assert.assertEquals(null, Main.validateCommand(incorrectStartingPosition4));
        System.out.println();
        Assert.assertEquals(null, Main.validateCommand(positiveStartingPosition5));
        System.out.println();
        testFooter(testName);
    }

    @Test
    public void testPositiveValidateMovementCommands(){
        String testName = "Correct Values for Rover Movement";
        testHeader(testName);
        Assert.assertNotNull(Main.validateCommand(positiveMovement));
        Assert.assertNotNull(Main.validateCommand(positiveMovement1));
        Assert.assertNotNull(Main.validateCommand(positiveMovement2));
        Assert.assertNotNull(Main.validateCommand(positiveMovement3));

        assertReflectionEquals(positiveMovementCR, Main.validateCommand(positiveMovement));
        assertReflectionEquals(positiveMovementCR1, Main.validateCommand(positiveMovement1));
        assertReflectionEquals(positiveMovementCR2, Main.validateCommand(positiveMovement2));
        assertReflectionEquals(positiveMovementCR3, Main.validateCommand(positiveMovement3));
        testFooter(testName);
    }

    @Test
    public void testNegativeValidateMovementCommands(){
        String testName = "Incorrect Values for Rover Movement";
        testHeader(testName);
        Assert.assertEquals(null, Main.validateCommand(incorrectMovement));
        System.out.println();
        Assert.assertEquals(null, Main.validateCommand(incorrectMovement1));
        System.out.println();
        Assert.assertEquals(null, Main.validateCommand(incorrectMovement2));
        System.out.println();
        Assert.assertEquals(null, Main.validateCommand(incorrectMovement3));
        System.out.println();
        testFooter(testName);
    }
}