package com.next45.rover;

import com.next45.IMove;
import com.next45.commands.ControlRoom;
import com.next45.direction.Direction;

/***
 * This class represents our Rover
 * @author Ndafara Tsamba
 */
public class Rover implements IMove {

    private int xCoordinateBounds;
    private int yCoordinateBounds;

    private int startingXCoordinate;
    private int startingYCoordinate;
    private Direction startingDirection;

    private char[] movements;

    public Rover(ControlRoom instructions){
        //Set the Bounds
        char[] zoneBounds = instructions.getZoneSize().toCharArray();
        xCoordinateBounds=Character.getNumericValue(zoneBounds[0]);
        yCoordinateBounds=Character.getNumericValue(zoneBounds[1]);

        //Set the Starting Coordinates
        char[] startingPositions = instructions.getStartingLocationAndOrientation().toCharArray();
        startingXCoordinate = Character.getNumericValue(startingPositions[0]);
        startingYCoordinate = Character.getNumericValue(startingPositions[1]);
        startingDirection = Direction.get(String.valueOf(startingPositions[3]));

        movements = instructions.getMovement().toCharArray();

    }

    @Override
    public String move() {
        for(char movemementCharacter : movements){
            switch (movemementCharacter){
                case 'M':
                    oneStep(startingDirection);
                    break;
                case 'R':
                    rotateRight(startingDirection);
                    break;
                case 'L':
                    rotateLeft(startingDirection);
                    break;
                    default:
                        System.out.println("Alien Interception of Movement Commands");
            }
        }
        return startingXCoordinate+""+startingYCoordinate+" "+startingDirection;
    }

    private void oneStep(Direction facedDirection){
        //Move One Step in the Direction being faced.
        switch(facedDirection){
            case N:
                setStartingYCoordinate(1);
                break;
            case S:
                setStartingYCoordinate(-1);
                break;
            case E:
                setStartingXCoordinate(1);
                break;
            case W:
                setStartingXCoordinate(-1);
                break;
                default:
                    System.out.println("Alien Interception of Direction");
        }
    }

    private void rotateRight(Direction facedDirection){
        //Rotate Right from Direction being faced.
        switch(facedDirection){
            case N:
                setStartingDirection(Direction.E);
                break;
            case S:
                setStartingDirection(Direction.W);
                break;
            case E:
                setStartingDirection(Direction.S);
                break;
            case W:
                setStartingDirection(Direction.N);
                break;
            default:
                System.out.println("Alien Interception of Direction");
        }
    }

    private void rotateLeft(Direction facedDirection){
        //Rotate Left from Direction being faced.
        switch(facedDirection){
            case N:
                setStartingDirection(Direction.W);
                break;
            case S:
                setStartingDirection(Direction.E);
                break;
            case E:
                setStartingDirection(Direction.N);
                break;
            case W:
                setStartingDirection(Direction.S);
                break;
            default:
                System.out.println("Alien Interception of Direction");
        }
    }


    public int getxCoordinateBounds() {
        return xCoordinateBounds;
    }

    public void setxCoordinateBounds(int xCoordinateBounds) {
        this.xCoordinateBounds = xCoordinateBounds;
    }

    public int getyCoordinateBounds() {
        return yCoordinateBounds;
    }

    public void setyCoordinateBounds(int yCoordinateBounds) {
        this.yCoordinateBounds = yCoordinateBounds;
    }

    public int getStartingXCoordinate() {
        return startingXCoordinate;
    }

    public void setStartingXCoordinate(int startingXCoordinate) {
        if(startingXCoordinate<0){
            this.startingXCoordinate--;
        }else{
            this.startingXCoordinate++;
        }
    }

    public int getStartingYCoordinate() {
        return startingYCoordinate;
    }

    public void setStartingYCoordinate(int startingYCoordinate) {
        if(startingYCoordinate<0){
            this.startingYCoordinate--;
        }else{
            this.startingYCoordinate++;
        }
    }

    public Direction getStartingDirection() {
        return startingDirection;
    }

    public void setStartingDirection(Direction startingDirection) {
        this.startingDirection = startingDirection;
    }

    public char[] getMovements() {
        return movements;
    }

    public void setMovements(char[] movements) {
        this.movements = movements;
    }
}
