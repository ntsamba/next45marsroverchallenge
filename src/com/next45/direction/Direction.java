package com.next45.direction;

/***
 * @author Ndafara Tsamba
 * This Enum holds the value of the Acceptable Directions
 */
public enum Direction {
    N,
    S,
    E,
    W,
    INVALID;

    public static Direction get(String value){
        Direction direction = null;

        try{
            direction = Direction.valueOf(value.toUpperCase());
        }catch(Exception exception){
            direction = Direction.INVALID;
        }

        return direction;
    }

}
