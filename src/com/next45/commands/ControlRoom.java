package com.next45.commands;

/***
 * @author Ndafara Tsamba
 * This class is used to create a command that is sent from the ControlRoom to the Rover
 */
public class ControlRoom {
    private String zoneSize;
    private String startingLocationAndOrientation;
    private String movement;

    public ControlRoom(String[] commandArray){
        setZoneSize(commandArray[0].trim());
        setStartingLocationAndOrientation(commandArray[1].trim());
        setMovement(commandArray[2].trim());
    }

    public String getZoneSize() {
        return zoneSize;
    }

    public void setZoneSize(String zoneSize) {
        this.zoneSize = zoneSize;
    }

    public String getStartingLocationAndOrientation() {
        return startingLocationAndOrientation;
    }

    public void setStartingLocationAndOrientation(String startingLocationAndOrientation) {
        this.startingLocationAndOrientation = startingLocationAndOrientation;
    }

    public String getMovement() {
        return movement;
    }

    public void setMovement(String movement) {
        this.movement = movement;
    }

}
