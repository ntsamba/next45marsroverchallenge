package com.next45;

import com.next45.commands.ControlRoom;
import com.next45.direction.Direction;
import com.next45.rover.Rover;

import java.util.Scanner;

/***
 * @author Ndafara Tsamba
 */

public class Main {

    public static void main(String[] args) {
        //When the program starts, get the commands and validate them
        ControlRoom controlRoomCommand = validateCommand(getCommands());
        if(controlRoomCommand!=null){
            //If the commands are validated, let us send them to the Rover.
            Rover moveRover = new Rover(controlRoomCommand);
            System.out.println(moveRover.move());
        }
    }

    /***
     * This method gets the input from the user to form the command that will be sent to the Rover
     */
    private static String[] getCommands() {
        Scanner command = new Scanner(System.in);
        String[] receivedCommand = new String[3];

        //We get three lines to make a command
        for (int i = 0; i < 3; i++) {
            receivedCommand[i] = command.nextLine().toUpperCase().trim();
        }
        return receivedCommand;
    }

    /**
     * This method validates the received commandArray to make sure it is correct
     * Assumptions:
     * The Origin is 0.0 so any size with negative is not accepted
     * The zone boundary input length is always divisible by 2 e.g 1290, 982190 are valid. 985, 67362 are not valid
     *
     * @param commandArray the string array with the received commands
     */
    public static ControlRoom validateCommand(String[] commandArray) {
        //validate Zone Parameter and assign it to validated
        boolean validated = false;

        if(validateZone(commandArray[0])){
            //Validate the Starting Location
            if(validateStartingPosition(commandArray[1])){
                //Validate the Movement Instructions
                if(validateMovementCommands(commandArray[2])){
                    //return null;
                }
                else{
                    return null;
                }
            }else{
                return null;
            }
        }else{
            //Return false because the first check is false
            return null;
        }

        //Create ControlRoom Object
        ControlRoom controlRoom = new ControlRoom(commandArray);
        return controlRoom;
    }

    /***
     * This method validates the first parameter in the Command List
     * @param zoneParameter
     * @return true if valid
     */
    private static boolean validateZone(String zoneParameter){
        boolean zoneValidated = false;
        //Validate the Zone
        int zoneLength = zoneParameter.length() % 2;
        if (zoneLength != 0) {
            //Zone is not valid. Return false.
            System.out.printf("The First Parameter for [Zone Size] : %s is not even.", zoneParameter);
            return zoneValidated;
        } else {
            //Check if every character is a digit
            String coordinates = zoneParameter;
            for (char eachCharacter : coordinates.toCharArray()) {
                if(!Character.isDigit(eachCharacter)){
                    System.out.printf("The First Parameter [Zone Size] : %s is not correct.", coordinates);
                    return zoneValidated;
                }
            }
        }
        return true;
    }

    /***
     * This method validates the Starting Position in in the Command List
     * @param startingPosition
     * @return true if valid
     */
    private static boolean validateStartingPosition(String startingPosition){
        boolean validated = false;
        String[] locationAndOrientation = startingPosition.split("\\s+");
        if(locationAndOrientation.length>2){
            System.out.printf("The [Starting Location] : %s for the Rover is not correct. Too many parameters.", startingPosition);
            return validated;
        }
        else
        {
            for(int i=0; i<locationAndOrientation.length; i++)
            {
                //Validate the parts
                if(i==0){
                    String coordinates = locationAndOrientation[0];
                    for (char eachCharacter : coordinates.toCharArray()) {
                        if(!Character.isDigit(eachCharacter)){
                            System.out.printf("The [Starting Location] : %s coordinates are not correct.", coordinates);
                            return validated;
                        }
                    }
                }else
                {
                    String cardinalPoints = locationAndOrientation[1];
                    if(cardinalPoints.length() > 1){
                        System.out.printf("The [Starting Location] : %s direction is not correct.", cardinalPoints);
                        return validated;
                    }else
                    {
                        Direction facedDirection = Direction.get(cardinalPoints);

                        if(facedDirection == Direction.INVALID){
                            System.out.printf("The [Starting Location] : %s direction is not correct.", cardinalPoints);
                            return validated;
                        }
                    }
                }
            }
        }
        return true;
    }

    /***
     * This method validates the Movement Commands in the Command List
     * @param movementCommands
     * @return true if valid
     */
    private static boolean validateMovementCommands(String movementCommands){
        //Validate the Movement Commands
        if(movementCommands.length()==0){
            System.out.printf("The [Movement Commands] : %s for the Rover are not correct.", movementCommands);
            return false;
        }else
        {
            char[] movement = movementCommands.toCharArray();

            for(char eachCharacter : movement){
                if(eachCharacter!='M' && eachCharacter!='L' && eachCharacter!='R'){
                    System.out.printf("The [Movement Commands] : %s for the Rover are not correct.", String.valueOf(movement));
                    return false;
                }
            }
        }
        return true;
    }
}

