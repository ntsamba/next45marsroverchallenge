package com.next45;

/***
 * This Interface is the contract used to make sure each Rover can move
 * @author Ndafara Tsamba
 */
public interface IMove {
    String move();
}
