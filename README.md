#Next45 Mars Rover Challenge

**Version 1.1.0**

This is my implementation of the Next45 Mars Rover Challenge as part of their hiring process.

---
##Installation and Getting Started

- This application was developed in Java in the Intellij IDE.
- It can easily be used in any IDE that supports Java.
- The Java version used is 1.8.0_144 but I am sure it compiles in lower versions.

---
#### To install this project
- Clone the project on your system.
- Extract the files.
- Open the extracted folder in your IDE.
- Run the Main Class.
- Input the commands as follows in the terminal:

   i. Put in the Zone Size e.g. 88 and press Enter
   
   ii. Put in the Starting Location e.g. 12 N and press Enter
   
   iii. Put in the Movement Instructions e.g. MMMLRLRLLRRMMM
   
   iv. The input is validated before being processed i.e. sent to the Rover.

---
##Design Decisions and Thought Process

The design decisions used in this project were based on the SOLID principles of OOP.

I used the TTD process during the development of this app. I wrote the tests first and then I ensured my applications
correctness by making sure it passes the tests.

- validated input before it got to the Rover.
- used Enums for the Cardinal points.
- Interface to enforce the Rover's move function.
- Classes for Commands and Rovers - we might have different sets of both in the future.

##### The Test Strategy

The test strategy was to write the Unit tests first and then do the implementation to satisfy the tests.
I did this because automated testing saves time as the project gets bigger as they guide you when you break something.
Also, I already had the acceptance criteria and used this as a means to know what needs to be tested. 
It also provides safer refactoring, you know what broke and where you need to fix because of the tests. E.g. when I changed
the return type for a method from boolean to an Object.

---
###Possible Extensions to the code
1. Use of Loggers instead of printing out to Standard output.
2. Reading a file with the instructions.
3. Validating size bounds to make sure the Rover doesn't fall over.

##Contributors

[Ndafara Keith Tsamba](https://bitbucket.org/ntsamba/)

---
##Licences
© Ndafara Keith Tsamba, [Next45](https://www.docfoxapp.com/)